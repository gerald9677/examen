/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoimpl;

import beans.Producto;
import dao.IDaoProducto;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.RandomFileBinarySearch;


/**
 *
 * @author Yasser
 */
public class IDaoImplProducto implements IDaoProducto {

    private RandomAccessFile hraf;
    private RandomAccessFile draf;
    private static int SIZE = 498;
    private final String HEADER_FILENAME = "hproducto.dat";
    private final String DATA_FILENAME = "dproducto.dat";

    public IDaoImplProducto() {
        RandomFileBinarySearch.SIZE = 4;
    }

    private void open() throws IOException {

        File hfile = new File(HEADER_FILENAME);
        File dfile = new File(DATA_FILENAME);

        if (!hfile.exists()) {
            hfile.createNewFile();
            hraf = new RandomAccessFile(hfile, "rw");
            draf = new RandomAccessFile(dfile, "rw");
            hraf.seek(0);
            hraf.writeInt(0);
            hraf.writeInt(0);
        } else {
            hraf = new RandomAccessFile(hfile, "rw");
            draf = new RandomAccessFile(dfile, "rw");
        }

    }

    private void close() throws IOException {
        if (hraf != null) {
            hraf.close();
            hraf = null;
        }

        if (draf != null) {
            draf.close();
            draf = null;
        }
    }

    @Override
    public Producto buscarPorId(int id) throws IOException {
        open();
        hraf.seek(0);
        int n = hraf.readInt();
        int k = hraf.readInt();
        if (n == 0) {
            close();
            return null;
        }

        RandomFileBinarySearch.hraf = hraf;
        int pos = RandomFileBinarySearch.runBinarySearchRecursively(id, 0, n);
        if (pos < 0) {
            close();
            return null;
        }
        long hpos = 8 + 4 * (pos);
        hraf.seek(hpos);
        int index = hraf.readInt();

        long dpos = (index - 1) * SIZE;
        draf.seek(dpos);

        Producto p = new Producto();
        p.setId(draf.readInt());
        p.setCodProducto(draf.readUTF());
        p.setNombre(draf.readUTF());
        p.setDescripcion(draf.readUTF());
        p.setCantidad(draf.readInt());
        p.setPrecio(draf.readDouble());
        p.setRutaimagen(draf.readUTF());

        close();

        return p;
    }

    @Override
    public Producto buscarPorCodProducto(String codProd) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Producto t) throws IOException {
        open();
        hraf.seek(0);
        int n = hraf.readInt();
        int k = hraf.readInt();

        long pos = k * SIZE;

        draf.seek(pos);

        draf.writeInt(++k);
        draf.writeUTF(t.getCodProducto());
        draf.writeUTF(t.getNombre());
        draf.writeUTF(t.getDescripcion());
        draf.writeInt(t.getCantidad());
        draf.writeDouble(t.getPrecio());
        draf.writeUTF(t.getRutaimagen());

        hraf.seek(0);
        hraf.writeInt(++n);
        hraf.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        hraf.seek(hpos);
        hraf.writeInt(k);
        close();
    }

    @Override
    public int update(Producto t) throws IOException {
        open();
        hraf.seek(0);
        RandomFileBinarySearch.hraf = hraf;
        int n = hraf.readInt();
        int pos = RandomFileBinarySearch.runBinarySearchRecursively(t.getId(), 0, n);
        if (pos < 0) {
            close();
            return pos;
        }
        long hpos = 8 + 4 * (pos);
        hraf.seek(hpos);
        int id = hraf.readInt();

        long dpos = (id - 1) * SIZE;
        draf.seek(dpos);

        draf.writeInt(t.getId());
        draf.writeUTF(t.getCodProducto());
        draf.writeUTF(t.getNombre());
        draf.writeUTF(t.getDescripcion());
        draf.writeInt(t.getCantidad());
        draf.writeDouble(t.getPrecio());
        draf.writeUTF(t.getRutaimagen());

        close();
        return t.getId();
    }

    @Override
    public boolean delete(Producto t) throws IOException {
        File tmp = new File("tmp.dat");
        try (RandomAccessFile tmpraf = new RandomAccessFile(tmp, "rw")) {
            open();
            hraf.seek(0);
            int n = hraf.readInt();
            int k = hraf.readInt();

            tmpraf.seek(0);
            tmpraf.writeInt(n - 1);
            tmpraf.writeInt(k);
            int j = 0;
            for (int i = 0; i < n; i++) {
                long hpos = 8 + 4 * i;
                hraf.seek(hpos);
                int id = hraf.readInt();
                if (id == t.getId()) {
                    continue;
                }
                long tmpos = 8 + 4 * j++;
                tmpraf.seek(tmpos);
                tmpraf.writeInt(id);
            }
            close();
        }
        close();
        File f = new File(HEADER_FILENAME);
        boolean flag = f.delete();
        if (flag) {
            tmp.renameTo(f);
        } else {
            Logger.getLogger(IDaoImplProducto.class.getName()).log(Level.SEVERE, "ERROR, no se pudo eliminar el archivo!");
        }
        return flag;
    }

    @Override
    public List<Producto> findAll() throws IOException {
        open();
        List<Producto> productos = new ArrayList<>();
        hraf.seek(0);
        int n = hraf.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            hraf.seek(hpos);
            int id = hraf.readInt();

            long dpos = (id - 1) * SIZE;
            draf.seek(dpos);

            Producto p = new Producto();
            p.setId(draf.readInt());
            p.setCodProducto(draf.readUTF());
            p.setNombre(draf.readUTF());
            p.setDescripcion(draf.readUTF());
            p.setCantidad(draf.readInt());
            p.setPrecio(draf.readDouble());
            p.setRutaimagen(draf.readUTF());

            productos.add(p);
        }

        close();
        return productos;
    }

}
